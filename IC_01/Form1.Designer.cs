﻿namespace IC_01
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.convertButton = new System.Windows.Forms.Button();
            this.earthWeightTextBox = new System.Windows.Forms.TextBox();
            this.earthWeight = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.marsWeightLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // convertButton
            // 
            this.convertButton.Location = new System.Drawing.Point(350, 335);
            this.convertButton.Name = "convertButton";
            this.convertButton.Size = new System.Drawing.Size(98, 49);
            this.convertButton.TabIndex = 0;
            this.convertButton.Text = "Convert";
            this.convertButton.UseVisualStyleBackColor = true;
            this.convertButton.Click += new System.EventHandler(this.ConvertButton_Click);
            // 
            // earthWeightTextBox
            // 
            this.earthWeightTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.earthWeightTextBox.Location = new System.Drawing.Point(234, 158);
            this.earthWeightTextBox.Name = "earthWeightTextBox";
            this.earthWeightTextBox.Size = new System.Drawing.Size(100, 26);
            this.earthWeightTextBox.TabIndex = 1;
            // 
            // earthWeight
            // 
            this.earthWeight.AutoSize = true;
            this.earthWeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.earthWeight.Location = new System.Drawing.Point(89, 164);
            this.earthWeight.Name = "earthWeight";
            this.earthWeight.Size = new System.Drawing.Size(139, 20);
            this.earthWeight.TabIndex = 2;
            this.earthWeight.Text = "Weight on Earth";
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(234, 262);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 26);
            this.textBox1.TabIndex = 3;
            this.textBox1.TextChanged += new System.EventHandler(this.TextBox1_TextChanged);
            // 
            // marsWeightLabel
            // 
            this.marsWeightLabel.AutoSize = true;
            this.marsWeightLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.marsWeightLabel.Location = new System.Drawing.Point(89, 268);
            this.marsWeightLabel.Name = "marsWeightLabel";
            this.marsWeightLabel.Size = new System.Drawing.Size(134, 20);
            this.marsWeightLabel.TabIndex = 4;
            this.marsWeightLabel.Text = "Weight on Mars";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(512, 450);
            this.Controls.Add(this.marsWeightLabel);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.earthWeight);
            this.Controls.Add(this.earthWeightTextBox);
            this.Controls.Add(this.convertButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button convertButton;
        private System.Windows.Forms.TextBox earthWeightTextBox;
        private System.Windows.Forms.Label earthWeight;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label marsWeightLabel;
    }
}

